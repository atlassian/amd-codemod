├─ abbrev@1.0.7
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/abbrev-js
│  └─ licenseFile: node_modules/abbrev/LICENSE
├─ acorn@1.2.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/marijnh/acorn
│  └─ licenseFile: node_modules/acorn/LICENSE
├─ align-text@0.1.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/align-text
│  └─ licenseFile: node_modules/align-text/LICENSE
├─ alter@0.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/alter
│  └─ licenseFile: node_modules/alter/LICENSE
├─ amd-codemod@1.0.0
│  ├─ licenses: Apache-2.0
│  ├─ repository: git://bitbucket.org/atlassian/amd-codemod
│  └─ licenseFile: LICENSE.txt
├─ amdefine@1.0.0
│  ├─ licenses: BSD-3-Clause AND MIT
│  ├─ repository: https://github.com/jrburke/amdefine
│  └─ licenseFile: node_modules/amdefine/LICENSE
├─ ansi-regex@2.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/ansi-regex
│  └─ licenseFile: node_modules/ansi-regex/license
├─ ansi-styles@1.0.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/ansi-styles
├─ ansi-styles@2.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/chalk/ansi-styles
│  └─ licenseFile: node_modules/ansi-styles/license
├─ argparse@1.0.6
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/nodeca/argparse
│  └─ licenseFile: node_modules/argparse/LICENSE
├─ assertion-error@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/assertion-error
├─ ast-traverse@0.1.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/ast-traverse
│  └─ licenseFile: node_modules/ast-traverse/LICENSE
├─ ast-types@0.8.12
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/benjamn/ast-types
│  └─ licenseFile: node_modules/regenerator/node_modules/ast-types/LICENSE
├─ ast-types@0.8.15
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/benjamn/ast-types
│  └─ licenseFile: node_modules/ast-types/LICENSE
├─ async@0.2.10
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/caolan/async
│  └─ licenseFile: node_modules/uglify-js/node_modules/async/LICENSE
├─ async@1.5.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/caolan/async
│  └─ licenseFile: node_modules/async/LICENSE
├─ babel-core@5.8.35
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel/babel
├─ babel-plugin-constant-folding@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-constant-folding
├─ babel-plugin-dead-code-elimination@1.0.2
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-dead-code-elimination
├─ babel-plugin-eval@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-eval
├─ babel-plugin-inline-environment-variables@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-inline-environment-variables
├─ babel-plugin-jscript@1.0.4
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-jscript
├─ babel-plugin-member-expression-literals@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-member-expression-literals
├─ babel-plugin-property-literals@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-property-literals
├─ babel-plugin-proto-to-assign@1.0.4
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-proto-to-assign
├─ babel-plugin-react-constant-elements@1.0.3
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-react-constant-elements
├─ babel-plugin-react-display-name@1.0.3
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-react-display-name
├─ babel-plugin-remove-console@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-remove-console
├─ babel-plugin-remove-debugger@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-remove-debugger
├─ babel-plugin-runtime@1.0.7
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-runtime
├─ babel-plugin-undeclared-variables-check@1.0.2
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-undeclared-variables-check
├─ babel-plugin-undefined-to-void@1.1.6
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel-plugins/babel-plugin-undefined-to-void
├─ babel-runtime@5.8.35
│  ├─ licenses: MIT
│  └─ repository: https://github.com/babel/babel
├─ babylon@5.8.35
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/babel/babel
│  └─ licenseFile: node_modules/babylon/LICENSE
├─ balanced-match@0.3.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/juliangruber/balanced-match
│  └─ licenseFile: node_modules/balanced-match/LICENSE.md
├─ bluebird@2.10.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/petkaantonov/bluebird
│  └─ licenseFile: node_modules/bluebird/LICENSE
├─ brace-expansion@1.1.3
│  ├─ licenses: MIT
│  └─ repository: https://github.com/juliangruber/brace-expansion
├─ breakable@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/breakable
│  └─ licenseFile: node_modules/breakable/LICENSE
├─ camelcase@1.2.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/camelcase
│  └─ licenseFile: node_modules/camelcase/license
├─ center-align@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/center-align
│  └─ licenseFile: node_modules/center-align/LICENSE
├─ chai@3.5.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/chai
├─ chai-as-promised@5.2.0
│  ├─ licenses: WTFPL
│  ├─ repository: https://github.com/domenic/chai-as-promised
│  └─ licenseFile: node_modules/chai-as-promised/LICENSE.txt
├─ chai-things@0.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/RubenVerborgh/Chai-Things
│  └─ licenseFile: node_modules/chai-things/LICENSE.md
├─ chalk@0.4.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/chalk
├─ chalk@1.1.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/chalk/chalk
│  └─ licenseFile: node_modules/chalk/license
├─ cli-color@1.1.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/cli-color
│  └─ licenseFile: node_modules/cli-color/LICENSE
├─ cliui@2.1.0
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/bcoe/cliui
│  └─ licenseFile: node_modules/cliui/LICENSE.txt
├─ color-convert@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/qix-/color-convert
│  └─ licenseFile: node_modules/color-convert/LICENSE
├─ commander@0.6.1
│  ├─ licenses: MIT*
│  └─ repository: https://github.com/visionmedia/commander.js
├─ commander@2.3.0
│  ├─ licenses: MIT*
│  └─ repository: https://github.com/visionmedia/commander.js
├─ commander@2.9.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/tj/commander.js
│  └─ licenseFile: node_modules/commander/LICENSE
├─ commoner@0.10.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/reactjs/commoner
│  └─ licenseFile: node_modules/commoner/LICENSE
├─ concat-map@0.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-concat-map
│  └─ licenseFile: node_modules/concat-map/LICENSE
├─ convert-source-map@1.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/thlorenz/convert-source-map
│  └─ licenseFile: node_modules/convert-source-map/LICENSE
├─ core-js@1.2.6
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/zloirock/core-js
│  └─ licenseFile: node_modules/core-js/LICENSE
├─ d@0.1.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/d
│  └─ licenseFile: node_modules/d/LICENCE
├─ debug@2.2.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/visionmedia/debug
├─ decamelize@1.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/decamelize
│  └─ licenseFile: node_modules/decamelize/license
├─ deep-eql@0.1.3
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/deep-eql
├─ deep-is@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/thlorenz/deep-is
│  └─ licenseFile: node_modules/deep-is/LICENSE
├─ defined@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/defined
│  └─ licenseFile: node_modules/defined/LICENSE
├─ defs@1.1.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/defs
│  └─ licenseFile: node_modules/defs/LICENSE
├─ detect-indent@3.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/detect-indent
│  └─ licenseFile: node_modules/detect-indent/license
├─ detective@4.3.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-detective
│  └─ licenseFile: node_modules/detective/LICENSE
├─ diff@1.4.0
│  ├─ licenses: BSD
│  └─ repository: https://github.com/kpdecker/jsdiff
├─ es5-ext@0.10.11
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/es5-ext
│  └─ licenseFile: node_modules/es5-ext/LICENSE
├─ es6-iterator@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/es6-iterator
│  └─ licenseFile: node_modules/es6-weak-map/node_modules/es6-iterator/LICENSE
├─ es6-iterator@2.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/es6-iterator
│  └─ licenseFile: node_modules/es6-iterator/LICENSE
├─ es6-promise@3.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jakearchibald/ES6-Promises
│  └─ licenseFile: node_modules/es6-promise/LICENSE
├─ es6-symbol@2.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/es6-symbol
│  └─ licenseFile: node_modules/es6-weak-map/node_modules/es6-symbol/LICENSE
├─ es6-symbol@3.0.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/es6-symbol
│  └─ licenseFile: node_modules/es6-symbol/LICENSE
├─ es6-weak-map@0.1.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/es6-weak-map
│  └─ licenseFile: node_modules/es6-weak-map/LICENCE
├─ escape-string-regexp@1.0.2
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/escape-string-regexp
├─ escape-string-regexp@1.0.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/escape-string-regexp
│  └─ licenseFile: node_modules/escape-string-regexp/license
├─ escodegen@1.7.1
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/estools/escodegen
│  └─ licenseFile: node_modules/escodegen/LICENSE.source-map
├─ esprima@1.2.5
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/ariya/esprima
│  └─ licenseFile: node_modules/escodegen/node_modules/esprima/LICENSE.BSD
├─ esprima@2.7.2
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/jquery/esprima
│  └─ licenseFile: node_modules/esprima/LICENSE.BSD
├─ esprima-fb@15001.1001.0-dev-harmony-fb
│  ├─ licenses: BSD
│  └─ repository: https://github.com/facebook/esprima
├─ estraverse@1.9.3
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/estools/estraverse
│  └─ licenseFile: node_modules/estraverse/LICENSE.BSD
├─ esutils@2.0.2
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/estools/esutils
│  └─ licenseFile: node_modules/esutils/LICENSE.BSD
├─ event-emitter@0.3.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/event-emitter
│  └─ licenseFile: node_modules/event-emitter/LICENSE
├─ fast-levenshtein@1.0.7
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/hiddentao/fast-levenshtein
│  └─ licenseFile: node_modules/fast-levenshtein/LICENSE.md
├─ fileset@0.2.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mklabs/node-fileset
│  └─ licenseFile: node_modules/fileset/LICENSE-MIT
├─ fs-readdir-recursive@0.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/fs-utils/fs-readdir-recursive
│  └─ licenseFile: node_modules/fs-readdir-recursive/LICENSE
├─ get-stdin@4.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/get-stdin
├─ glob@3.2.3
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/isaacs/node-glob
│  └─ licenseFile: node_modules/mocha/node_modules/glob/LICENSE
├─ glob@5.0.15
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-glob
│  └─ licenseFile: node_modules/glob/LICENSE
├─ globals@6.4.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/globals
│  └─ licenseFile: node_modules/globals/license
├─ graceful-fs@2.0.3
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/isaacs/node-graceful-fs
│  └─ licenseFile: node_modules/mocha/node_modules/graceful-fs/LICENSE
├─ graceful-fs@4.1.3
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-graceful-fs
│  └─ licenseFile: node_modules/graceful-fs/LICENSE
├─ graceful-readlink@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/zhiyelee/graceful-readlink
│  └─ licenseFile: node_modules/graceful-readlink/LICENSE
├─ growl@1.8.1
│  ├─ licenses: MIT*
│  └─ repository: https://github.com/visionmedia/node-growl
├─ handlebars@4.0.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/wycats/handlebars.js
│  └─ licenseFile: node_modules/handlebars/LICENSE
├─ has-ansi@2.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/has-ansi
│  └─ licenseFile: node_modules/has-ansi/license
├─ has-color@0.1.7
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/has-color
├─ has-flag@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/has-flag
│  └─ licenseFile: node_modules/has-flag/license
├─ home-or-tmp@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/home-or-tmp
│  └─ licenseFile: node_modules/home-or-tmp/license
├─ iconv-lite@0.4.13
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/ashtuchkin/iconv-lite
│  └─ licenseFile: node_modules/iconv-lite/LICENSE
├─ inflight@1.0.4
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/inflight
│  └─ licenseFile: node_modules/inflight/LICENSE
├─ inherits@2.0.1
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/inherits
│  └─ licenseFile: node_modules/inherits/LICENSE
├─ invert-kv@1.0.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/invert-kv
├─ is-absolute@0.1.7
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/is-absolute
│  └─ licenseFile: node_modules/is-absolute/LICENSE
├─ is-buffer@1.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/feross/is-buffer
│  └─ licenseFile: node_modules/is-buffer/LICENSE
├─ is-finite@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/is-finite
│  └─ licenseFile: node_modules/is-finite/license
├─ is-integer@1.0.6
│  ├─ licenses: WTFPL
│  ├─ repository: https://github.com/parshap/js-is-integer
│  └─ licenseFile: node_modules/is-integer/LICENSE
├─ is-relative@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/is-relative
│  └─ licenseFile: node_modules/is-relative/LICENSE-MIT
├─ isexe@1.1.2
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/isexe
│  └─ licenseFile: node_modules/isexe/LICENSE
├─ istanbul@0.4.2
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/gotwarlost/istanbul
│  └─ licenseFile: node_modules/istanbul/LICENSE
├─ jade@0.26.3
│  ├─ licenses: MIT*
│  ├─ repository: https://github.com/visionmedia/jade
│  └─ licenseFile: node_modules/jade/LICENSE
├─ js-tokens@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/lydell/js-tokens
│  └─ licenseFile: node_modules/js-tokens/LICENSE
├─ js-yaml@3.5.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/nodeca/js-yaml
│  └─ licenseFile: node_modules/js-yaml/LICENSE
├─ jscodeshift@0.3.14
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/facebook/jscodeshift
│  └─ licenseFile: node_modules/jscodeshift/LICENSE
├─ jsesc@0.5.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mathiasbynens/jsesc
│  └─ licenseFile: node_modules/jsesc/LICENSE-MIT.txt
├─ json5@0.4.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/aseemk/json5
├─ kind-of@3.0.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/kind-of
│  └─ licenseFile: node_modules/kind-of/LICENSE
├─ lazy-cache@1.0.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/lazy-cache
│  └─ licenseFile: node_modules/lazy-cache/LICENSE
├─ lcid@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/lcid
│  └─ licenseFile: node_modules/lcid/license
├─ left-pad@0.0.3
│  ├─ licenses: BSD
│  └─ repository: https://github.com/azer/left-pad
├─ leven@1.0.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/leven
│  └─ licenseFile: node_modules/leven/license
├─ levn@0.2.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/levn
│  └─ licenseFile: node_modules/levn/LICENSE
├─ line-numbers@0.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/lydell/line-numbers
│  └─ licenseFile: node_modules/line-numbers/LICENSE
├─ lodash@3.10.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/lodash/lodash
│  └─ licenseFile: node_modules/lodash/LICENSE
├─ longest@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/longest
│  └─ licenseFile: node_modules/longest/LICENSE
├─ lru-cache@2.7.3
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-lru-cache
│  └─ licenseFile: node_modules/lru-cache/LICENSE
├─ lru-queue@0.1.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/lru-queue
│  └─ licenseFile: node_modules/lru-queue/LICENCE
├─ memoizee@0.3.9
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/memoize
│  └─ licenseFile: node_modules/memoizee/LICENSE
├─ minimatch@0.2.14
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/isaacs/minimatch
│  └─ licenseFile: node_modules/mocha/node_modules/minimatch/LICENSE
├─ minimatch@2.0.10
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/minimatch
│  └─ licenseFile: node_modules/minimatch/LICENSE
├─ minimist@0.0.10
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/minimist
│  └─ licenseFile: node_modules/minimist/LICENSE
├─ minimist@0.0.8
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/minimist
│  └─ licenseFile: node_modules/mkdirp/node_modules/minimist/LICENSE
├─ minimist@1.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/minimist
│  └─ licenseFile: node_modules/detect-indent/node_modules/minimist/LICENSE
├─ mkdirp@0.3.0
│  ├─ licenses: MIT/X11
│  ├─ repository: https://github.com/substack/node-mkdirp
│  └─ licenseFile: node_modules/jade/node_modules/mkdirp/LICENSE
├─ mkdirp@0.5.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-mkdirp
│  └─ licenseFile: node_modules/mkdirp/LICENSE
├─ mocha@2.4.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mochajs/mocha
│  └─ licenseFile: node_modules/mocha/LICENSE
├─ ms@0.7.1
│  ├─ licenses: MIT*
│  ├─ repository: https://github.com/guille/ms.js
│  └─ licenseFile: node_modules/ms/LICENSE
├─ next-tick@0.2.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/next-tick
│  └─ licenseFile: node_modules/next-tick/LICENCE
├─ node-dir@0.1.8
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/fshost/node-dir
│  └─ licenseFile: node_modules/node-dir/LICENSE.txt
├─ nomnom@1.8.1
│  ├─ licenses: MIT*
│  ├─ repository: https://github.com/harthur/nomnom
│  └─ licenseFile: node_modules/nomnom/LICENSE
├─ nopt@3.0.6
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/npm/nopt
│  └─ licenseFile: node_modules/nopt/LICENSE
├─ number-is-nan@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/number-is-nan
│  └─ licenseFile: node_modules/number-is-nan/license
├─ once@1.3.3
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/once
│  └─ licenseFile: node_modules/once/LICENSE
├─ optimist@0.6.1
│  ├─ licenses: MIT/X11
│  ├─ repository: https://github.com/substack/node-optimist
│  └─ licenseFile: node_modules/optimist/LICENSE
├─ optionator@0.5.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/optionator
│  └─ licenseFile: node_modules/optionator/LICENSE
├─ os-locale@1.4.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/os-locale
│  └─ licenseFile: node_modules/os-locale/license
├─ os-tmpdir@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/os-tmpdir
│  └─ licenseFile: node_modules/os-tmpdir/license
├─ output-file-sync@1.1.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/shinnn/output-file-sync
│  └─ licenseFile: node_modules/output-file-sync/LICENSE
├─ path-exists@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/path-exists
│  └─ licenseFile: node_modules/path-exists/license
├─ path-is-absolute@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/path-is-absolute
│  └─ licenseFile: node_modules/path-is-absolute/license
├─ prelude-ls@1.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/prelude-ls
│  └─ licenseFile: node_modules/prelude-ls/LICENSE
├─ private@0.1.6
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/benjamn/private
│  └─ licenseFile: node_modules/private/LICENSE
├─ q@1.4.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/kriskowal/q
│  └─ licenseFile: node_modules/q/LICENSE
├─ recast@0.10.33
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/benjamn/recast
│  └─ licenseFile: node_modules/regenerator/node_modules/recast/LICENSE
├─ recast@0.10.43
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/benjamn/recast
│  └─ licenseFile: node_modules/commoner/node_modules/recast/LICENSE
├─ recast@0.11.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/benjamn/recast
│  └─ licenseFile: node_modules/recast/LICENSE
├─ regenerate@1.2.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mathiasbynens/regenerate
│  └─ licenseFile: node_modules/regenerate/LICENSE-MIT.txt
├─ regenerator@0.8.40
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/facebook/regenerator
│  └─ licenseFile: node_modules/regenerator/LICENSE
├─ regexpu@1.3.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mathiasbynens/regexpu
│  └─ licenseFile: node_modules/regexpu/LICENSE-MIT.txt
├─ regjsgen@0.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/d10/regjsgen
│  └─ licenseFile: node_modules/regjsgen/LICENSE.txt
├─ regjsparser@0.1.5
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/jviereck/regjsparser
│  └─ licenseFile: node_modules/regjsparser/LICENSE.BSD
├─ repeat-string@1.5.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/repeat-string
│  └─ licenseFile: node_modules/repeat-string/LICENSE
├─ repeating@1.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/repeating
│  └─ licenseFile: node_modules/repeating/license
├─ resolve@1.1.7
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-resolve
│  └─ licenseFile: node_modules/resolve/LICENSE
├─ right-align@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/right-align
│  └─ licenseFile: node_modules/right-align/LICENSE
├─ shebang-regex@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/shebang-regex
│  └─ licenseFile: node_modules/shebang-regex/license
├─ sigmund@1.0.1
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/sigmund
│  └─ licenseFile: node_modules/sigmund/LICENSE
├─ simple-fmt@0.1.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/simple-fmt
│  └─ licenseFile: node_modules/simple-fmt/LICENSE
├─ simple-is@0.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/simple-is
│  └─ licenseFile: node_modules/simple-is/LICENSE
├─ slash@1.0.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/slash
├─ source-map@0.1.32
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/mozilla/source-map
│  └─ licenseFile: node_modules/source-map-support/node_modules/source-map/LICENSE
├─ source-map@0.2.0
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/mozilla/source-map
│  └─ licenseFile: node_modules/source-map/LICENSE
├─ source-map@0.4.4
│  ├─ licenses: BSD-3-Clause
│  └─ repository: https://github.com/mozilla/source-map
├─ source-map@0.5.3
│  ├─ licenses: BSD-3-Clause
│  └─ repository: https://github.com/mozilla/source-map
├─ source-map-support@0.2.10
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/evanw/node-source-map-support
│  └─ licenseFile: node_modules/source-map-support/LICENSE.md
├─ sprintf-js@1.0.3
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/alexei/sprintf.js
│  └─ licenseFile: node_modules/sprintf-js/LICENSE
├─ stable@0.1.5
│  ├─ licenses: MIT
│  └─ repository: https://github.com/Two-Screen/stable
├─ stringmap@0.2.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/stringmap
│  └─ licenseFile: node_modules/stringmap/LICENSE
├─ stringset@0.2.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/stringset
│  └─ licenseFile: node_modules/stringset/LICENSE
├─ strip-ansi@0.1.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/strip-ansi
├─ strip-ansi@3.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/chalk/strip-ansi
│  └─ licenseFile: node_modules/strip-ansi/license
├─ supports-color@1.2.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/supports-color
├─ supports-color@2.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/chalk/supports-color
│  └─ licenseFile: node_modules/chalk/node_modules/supports-color/license
├─ supports-color@3.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/chalk/supports-color
│  └─ licenseFile: node_modules/supports-color/license
├─ through@2.3.8
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/dominictarr/through
│  └─ licenseFile: node_modules/through/LICENSE.MIT
├─ timers-ext@0.1.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/medikoo/timers-ext
│  └─ licenseFile: node_modules/timers-ext/LICENCE
├─ to-fast-properties@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/to-fast-properties
│  └─ licenseFile: node_modules/to-fast-properties/license
├─ trim-right@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/trim-right
│  └─ licenseFile: node_modules/trim-right/license
├─ try-resolve@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sebmck/try-resolve
├─ tryor@0.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/olov/tryor
│  └─ licenseFile: node_modules/tryor/LICENSE
├─ type-check@0.3.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/type-check
│  └─ licenseFile: node_modules/type-check/LICENSE
├─ type-detect@0.1.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/type-detect
├─ type-detect@1.0.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/type-detect
├─ uglify-js@2.6.2
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/mishoo/UglifyJS2
│  └─ licenseFile: node_modules/uglify-js/LICENSE
├─ uglify-to-browserify@1.0.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/ForbesLindesay/uglify-to-browserify
│  └─ licenseFile: node_modules/uglify-to-browserify/LICENSE
├─ underscore@1.6.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jashkenas/underscore
│  └─ licenseFile: node_modules/underscore/LICENSE
├─ user-home@1.1.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/user-home
│  └─ licenseFile: node_modules/user-home/license
├─ which@1.2.4
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-which
│  └─ licenseFile: node_modules/which/LICENSE
├─ window-size@0.1.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/window-size
│  └─ licenseFile: node_modules/window-size/LICENSE-MIT
├─ window-size@0.1.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/window-size
│  └─ licenseFile: node_modules/defs/node_modules/window-size/LICENSE
├─ wordwrap@0.0.2
│  ├─ licenses: MIT/X11
│  └─ repository: https://github.com/substack/node-wordwrap
├─ wordwrap@0.0.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-wordwrap
│  └─ licenseFile: node_modules/optionator/node_modules/wordwrap/LICENSE
├─ wordwrap@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-wordwrap
│  └─ licenseFile: node_modules/wordwrap/LICENSE
├─ wrappy@1.0.1
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/npm/wrappy
│  └─ licenseFile: node_modules/wrappy/LICENSE
├─ xtend@4.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/Raynos/xtend
│  └─ licenseFile: node_modules/xtend/LICENCE
├─ y18n@3.2.0
│  ├─ licenses: ISC
│  └─ repository: https://github.com/bcoe/y18n
├─ yargs@3.10.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/bcoe/yargs
│  └─ licenseFile: node_modules/yargs/LICENSE
└─ yargs@3.27.0
   ├─ licenses: MIT
   ├─ repository: https://github.com/bcoe/yargs
   └─ licenseFile: node_modules/defs/node_modules/yargs/LICENSE
