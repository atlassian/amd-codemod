v1.2.0 - April 11, 2016

* 1.2.0 (Chris Darroch)
* Refactored code to put each jscodeshift plugin and transform function in to its own file (Chris Darroch)
* Added #getAMDDefineCalls, #getAMDRequireCalls, and #getAMDCalls filtering functions (#3) (Chris Darroch)
* Support for rewriting require() blocks (#5) (Bradley Ayers)
* Support for rewriting anonymous define() blocks (#5) (Bradley Ayers, Chris Darroch)
* Added a new #toBodyStrategy transform function (Sergio Cinos)
* Bugfix: injecting dependencies with the body strategy will put the vars after a 'use strict' statement (#1) (Sergio Cinos)

v1.1.0 - March 30, 2016

* 1.1.0 (Chris Darroch)
* Refactor of internal logic for detecting globals to be replaced (Chris Darroch)
* The #toAMD function supports splitting instance members when replacing globals (Chris Darroch)
* Docs: added example usages of the #toAMD function (Chris Darroch)

v1.0.0 - March 09, 2016

* 1.0.0 (Dick Wiggers)
* Added #toAMD function to codemod (Dick Wiggers)
* Added #isAMDDefine function to codemod (Dick Wiggers)
* Test cases and examples for common usages added (Dick Wiggers)
* README added (Dick Wiggers)
* Licenses for dependencies added (Dick Wiggers)
