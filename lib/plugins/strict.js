"use strict";
const types = require("ast-types").namedTypes

function isUseStrict(path) {
    const node = path.node;
    const parentNode = path.parent.node;

    return types.BlockStatement.check(parentNode)
        && types.ExpressionStatement.check(node)
        && types.Literal.check(node.expression)
        && node.expression.value == "use strict";
}

module.exports = j => {
    j.registerMethods({
        findUseStrict() {
            return this.filter(isUseStrict);
        }
    });
}