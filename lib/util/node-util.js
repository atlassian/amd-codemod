function NodeUtil(jscodeshift) {
    this.j = jscodeshift;
}

function isPropertyOf(parent, node) {
    return parent
        && parent.type === 'MemberExpression'
        && parent.property === node;
}

function isRootOfCall(path) {
    var node = path.value;
    var parent = path.parent && path.parent.value;
    var isRoot = false;

    switch (node.type) {
        case 'MemberExpression':
            isRoot = true;
            break;
        case 'Identifier':
            isRoot = (!parent || !isPropertyOf(parent, node));
            break;
    }
    return isRoot;
}

NodeUtil.prototype.isUsageOf = function(name) {
    var j = this.j;
    return function testPathForMember(path) {
        return isRootOfCall(path)
            && j(path).toSource() === name;
    }
}

NodeUtil.prototype.isCallOf = function(name) {
    var j = this.j;
    return function testPathForCall(path) {
        return path.value.type === 'CallExpression'
            && isRootOfCall(path.get('callee'))
            && j(path.get('callee')).toSource() === name;
    }
}

module.exports = NodeUtil;
