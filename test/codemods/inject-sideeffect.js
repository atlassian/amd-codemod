"use strict";
module.exports = function(fileInfo, api) {
    var j = api.jscodeshift;
    require("../../index")(j);
    var body = require("../../lib/strategy/body");
    var src = j(fileInfo.source);

    var defineBlocks = src.getAMDCalls()

    defineBlocks.forEach(function(path) {
        var strat = body(j, path);
        strat.addDependency("some-wicked-super-super-super-super-extra-really-really-very-very-very-super-duper-long-module-name-that-is-more-than-seventy-four-characters");
    });

    return src.toSource();
};
