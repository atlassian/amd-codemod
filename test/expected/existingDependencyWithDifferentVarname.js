define("test/input/existingDependencyWithDifferentVarname", ["some/dep", "what/the/foo", "some/other/dep"], function(somedep, whatthefoo, someotherdep) {
    whatthefoo.setOptions({ // <-- var should still be named 'whatthefoo'                                                     ^^^^^^^^^^- here too
        something: true
    });
});
