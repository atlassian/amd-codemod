define("test/input/jqueryVarFunc", ["jquery"], function(jQuery) {
    var url = "http://example.com/";
    jQuery("#main").data('link', url);

    jQuery.ajax({
        method: 'GET',
        url: jQuery("#main").data('link'),
        success: function() {}
    })
});
