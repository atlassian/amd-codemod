//noinspection JSUnusedLocalSymbols
define("test/input/requireStrategyPerLine3", ["require"], function(require) {
    var Bazfoo = require("baz/foo");
    var baz = require("b/a/z");
    var bar = require("b/a/r");
    var foo = require("f/o/o");
    // require() lines should show above this comment.
    foo.bar();
    bar.baz();
    baz.wtf();
    Bazfoo(bar, baz);

    //noinspection JSUnusedLocalSymbols
    var nestedBody = function() {
        bar.fly();
    };
});
