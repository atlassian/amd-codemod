define("test/input/requireStrategyWithUseStrict", ["require"], function(require) {
    "use strict";
    var foo = require("f/o/o");
    var a = "use strict";
    alert("use strict");
    foo.bar();
});
