define('test/data/variables', ["b/a/z", "b/a/r", "f/o/o"], function(baz, bar, foo) {
    foo.bar();
    foo.baz();

    function innerfunc() {
        bar.baz(foo);

        return baz.toString();
    }

    innerfunc();
});