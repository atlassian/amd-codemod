define("a/module", ["dep/a", "dep/b", "dep/c"], function(depA, depB, depC) {
    "use strict";
    depA.foo();
    depB(depC);
});
