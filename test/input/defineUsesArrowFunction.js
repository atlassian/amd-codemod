define(() => {
    F.O.O.bar("whatever");
});

define("uses-arow-function", () => {
    F.O.O.bar("whatever");
});

define([], () => {
    F.O.O.bar("whatever");
});

define("uses-arow-function", [], () => {
    F.O.O.bar("whatever");
});
