define('test/data/function', [], function() {
    foobar(barbaz(true), bazfoo(new Exception("yolo")), true);

    Leave.me.alone();

    bazfoo({}, [], "", false).each(function(x,y) {
        barbaz(x).get(y).init();

        Cant.touch.this();
    });
});
