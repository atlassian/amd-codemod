define("test/input/jqueryVarFunc", function() {
    var url = "http://example.com/";
    $("#main").data('link', url);

    $.ajax({
        method: 'GET',
        url: $("#main").data('link'),
        success: function() {}
    })
});
