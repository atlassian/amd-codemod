//noinspection JSUnusedLocalSymbols
define("test/input/nestedRequires", ["require"], function(require) {
    // require() lines should show above this comment.
    return {
        someFunction() {
            foo.bar();
            bar.baz();
            baz.wtf();
            Bazfoo(bar, baz);
        },
        someOtherFunction() {
            require("crap");
        }
    }
});
