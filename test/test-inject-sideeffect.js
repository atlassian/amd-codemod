"use strict";
const assertTransformFactory = require('./utils/assert-transform.js');
const cleanup = require('./utils/cleanup.js');

describe("Strategy: inject side-effect dependency", function() {
    after(cleanup);

    const assertTransform = assertTransformFactory("codemods/inject-sideeffect.js");

    it("should inject a dependency as a sideeffect into the function body", function() {
        return assertTransform("add-sideeffect-dependency.js");
    });
});
