"use strict";
const assertTransformFactory = require('./utils/assert-transform.js');
const cleanup = require('./utils/cleanup.js');

describe("Transform: toBodyStrategy", function() {
    after(cleanup);

    const assertTransform = assertTransformFactory("codemods/body-strategy.js");

    it("should move module dependencies in to the factory function's body", function() {
        return assertTransform("convertToBodyStrategy.js");
    });
});
