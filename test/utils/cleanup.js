"use strict";
const fs = require('fs');

module.exports = function() {
    const actual = "test/actual/";
    fs.readdirSync(actual).forEach(function (fileName) {
        if(fileName === ".gitignore") return;
        fs.unlinkSync(actual + fileName);
    });
};